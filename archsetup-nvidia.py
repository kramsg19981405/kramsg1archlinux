#!/usr/bin/python

import os

os.system('pacman -Syy wget')

# importing pacman.conf
os.system('mkdir /pacmanconf')
os.system ('cd /pacmanconf')
os.system('wget https://gitlab.com/kramsg19981405/kramsg1archlinux/-/blob/archsetup-pacman.conf/pacman.conf')
os.system('mv /pacmanconf/pacman.conf >> /etc/pacman.conf')
os.system('cd')

# changing mirrors
os.system('cd /etc/pacman.d/')
os.system('rm mirrorlist')
os.system('wget -o mirrorlist https://bit.ly/3cko2zr')
os.system('cd')

# synchronising mirrors
os.system('pacman -Syy')

# installing git
os.system('pacman -Syy git')

# checking for updates
os.system('pacman -Syyu')

# installing lts kernel
os.system('pacman -Syy linux-lts linux-lts-headers')

# installing nvidia drivers and vulkan support
os.system('pacman -Syy nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader')

# installing xorg 
os.system('pacman -Syy xorg')

# installing desktop environment
os.system('pacman -Syy plasma sddm')
os.system(' systemctl enable sddm')

# installing aur support
os.system('pacman -Syy yay')

# installing wine-staging
os.system('pacman -Syy wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader')

# installing lutris
os.system('pacman -Syy lutris')

# installing steam
os.system('pacman -Syy steam')
 
# installing extras
os.system('yay -Syyu')
os.system('yay -S google-chrome dxvk-bin obs-studio vlc')
 
