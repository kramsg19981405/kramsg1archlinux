sed -i -e 's/#en_US\.UTF-8 UTF-8/en_US\.UTF-8 UTF-8/' /etc/locale.gen
sed -i -e 's/#ru_RU\.UTF-8 UTF-8/ru_RU\.UTF-8 UTF-8/' /etc/locale.gen
sed -i -e 's/#he_IL\.UTF-8 UTF-8/he_IL\.UTF-8 UTF-8/' /etc/locale.gen

locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# TIMEZONE
ln -s /usr/share/zoneinfo/Asia/Jerusalem /etc/localtime
hwclock --systohc --utc


# BOOTLOADER
[GPT]
  bootctl install
  mple
  cp /usr/share/systemd/bootctl/arch.conf /boot/loader/entries

  PARTUUID = $(blkid -s PARTUUID -o value /dev/sda3)
  sed -i 's@rootfstype=XXXX@rootfstype=auto@' /boot/loader/entries/arch.conf
  sed -i "s@XXXX@$PARTUUID@" /boot/loader/entries/arch.conf


# SETTING ROOT PASSWORD
passwd

# EXIT CHROOT env
exit

# UNMOUNT PARTITIONS
umount -R /mnt

# REBOOT
reboot
