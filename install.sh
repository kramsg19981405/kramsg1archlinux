#################################################################
############################## DRAFT ############################
#################################################################
# arch linux - stage_install

# update system clock
$ timedatectl set-ntp true

# PARTITIONING (EFI)
-s -- never prompt the user
// mkpart [part-type fs-type name] start end

// create partition table
  // create GPT table on /dev/sda
  parted -s /dev/sda mklabel gpt

  // create MBR table on /dev/sda
  parted -s /dev/sda mklabel msdos

// Q. The resulting partition is not properly aligned for best performance.
// http://rainbow.chard.org/2013/01/30/how-to-align-partitions-for-best-performance-using-parted/
// man parted - https://www.gnu.org/software/parted/manual/parted.html
// make parition of type linux-swap 2GB
parted -s /dev/sda mkpart primary linux-swap 512 2000

/boot
  // make partition of type ext2 512MB (/boot) (GPT)
  parted -s /dev/sda mkpart primary fat32 2000 2512

  make partition of type ext2 512MB (/boot) (MBR)
  parted -s /dev/sda mkpart primary ext2 2000 2512

// make partition of type ext2 (/)
parted -s /dev/sda mkpart primary ext2 2512 100%

// The following command will be used to flag the partition that contains the /boot directory as bootable:
parted -s /dev/sda set 2 boot on

# FORMATTING
// /dev/sda1 - 2GB - swap
// /dev/sda2 - 512MB - /boot
// /dev/sda3 - remaining - /

mkfs.ext4 /dev/sda3
mkfs.fat -F32 /dev/sda2 (GPT)
mkfs.ext4 /dev/sda2 (MBR)
mkswap /dev/sda1
swapon /dev/sda1

# MOUNTING
mount /dev/sda3 /mnt/
mkdir -p /mnt/boot
mount /dev/sda2 /mnt/boot

# SELECTING PACKAGE MIRRORS
// backup the original mirrorlist file
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
echo "Server = http://mirror.isoc.org.il/pub/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist

// we should copy the backup of mirrorlist in to the new environment before chrooting
cp /etc/pacman.d/mirrorlist.backup /mnt/etc/pacman.d/

# INSTALL BASE PACKAGES
pacstrap /mnt base base-devel

# CONFIGURATION
// generate fstab
genfstab -U /mnt > /mnt/etc/fstab
cat /mnt/etc/fstab

// change root
arch-chroot /mnt /bin/bash

// setting locale
// http://unix.stackexchange.com/questions/159367/using-sed-to-find-and-replace
// en_US.UTF-8 UTF-8
// he_IL.UTF-8 UTF-8
// ru_RU.UTF-8 UTF-8
sed -i -e 's/#en_US\.UTF-8 UTF-8/en_US\.UTF-8 UTF-8/' /etc/locale.gen


locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# TIMEZONE
ln -s /usr/share/zoneinfo/Asia/Jerusalem /etc/localtime
hwclock --systohc --utc


# BOOTLOADER
[GPT]
  bootctl install
  // creating boot entry
  // copy an example
  cp /usr/share/systemd/bootctl/arch.conf /boot/loader/entries

  // getting root (/) partition UUID
  // should it be rw?
  PARTUUID = $(blkid -s PARTUUID -o value /dev/sda3)
  sed -i 's@rootfstype=XXXX@rootfstype=auto@' /boot/loader/entries/arch.conf
  sed -i "s@XXXX@$PARTUUID@" /boot/loader/entries/arch.conf

[MBR]
  pacman -S grub os-prober (MBR)
  grub-install /dev/sda (MBR)
  grub-mkconfig -o /boot/grub/grub.cfg // generate grub.cfg
  // /run/lvm/lvmetad.socket warning:  https://bbs.archlinux.org/viewtopic.php?id=174401

# NETWORK
// systemctl stop dhcpcd@{INTERFACE}.service
// getting active network interface name
// ACTIVE_NETWORK_INTERFACE=$(ip route get 8.8.8.8 | awk '{print $5; exit}')
// systemctl stop dhcpcd@enp0s25.service
echo 'devstation' >> /etc/hostname

// setting host in /etc/hosts is recommended (but we will skip)

// enable wired interface
// syntax: systemctl enable dhcpcd@[active-interface-name].service
ACTIVE_NETWORK_INTERFACE=$(ip route get 8.8.8.8 | awk '{print $5; exit}')
systemctl enable dhcpcd@$ACTIVE_NETWORK_INTERFACE.service

# SETTING ROOT PASSWORD
passwd

# EXIT CHROOT env
exit

# UNMOUNT PARTITIONS
umount -R /mnt

# REBOOT
reboot

----------------------------------

